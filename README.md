# OpenML dataset: Web-Series-Ultimate-Collection

https://www.openml.org/d/43453

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Content
This is a huge dataset that contains every web series around the globe streaming right now at the date of the creation of the dataset.
Inspiration
This dataset can be used to answer the following questions:

Which streaming platform(s) can I find this web series on?
Average IMDb rating and other ratings
What is the genre of the title?
What is the synopsis?
How many seasons are there right now?
Which year this was produced?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43453) of an [OpenML dataset](https://www.openml.org/d/43453). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43453/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43453/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43453/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

